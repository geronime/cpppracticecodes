// C++  program that demonstrates Multiple Inheritance

#include <iostream>
using namespace std;

// Base class 1
class Vehicle {
	
public:
	Vehicle() {
		cout << "This is a vehicle" << endl;
	}
};

// Base class 2
class FourWheeler {
	
public:
	FourWheeler() {
		cout << "This is a 4 wheeler vehicle" << endl;
	}
};

// Derived class inheriting two base classes, class 1 and class 2
class Car : public Vehicle, public FourWheeler {

};

// Main function
int main() {
	// creating object of derived class will invoke the
	// constructors of base class 1 and base class 2
	Car objCar;

	return 0;
}