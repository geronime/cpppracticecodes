#include <iostream>
using namespace std;

//Base class
class Parent {

public:
	int iId_p = 0;
};

// Sub class inheriting from base class
class Child : public Parent {

public:
	int iId_c = 0;
};

// Main function
int main() {
	Child childObject;

	// An object of class Child has all data members and
	// member functions from Parent class
	childObject.iId_c = 7;
	childObject.iId_p = 21;

	cout << "Child ID is " << childObject.iId_c << endl;
	cout << "Parent ID is " << childObject.iId_p << endl;
}