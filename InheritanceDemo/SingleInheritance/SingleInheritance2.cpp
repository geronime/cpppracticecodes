#include <iostream>
using namespace std;

// Base class
class Vehicle {
	
public:
	Vehicle() {
		cout << "This is a Vehicle" << endl;
	}
};

// Subclass derived from single base class
class Car : public Vehicle {

};

// Main function
int main() {
	// Creating object of derived class will invoke
	// the constructor of base calss
	Car objCar;
	return 0;
}