// Program to demonstrate Multi-Level Inheritance

#include <iostream>
using namespace std;

// Base class
class Vehicle {

public:
	Vehicle() {
		cout << "This is a vehicle" << endl;
	}
};

// Derived class 1
class FourWheeler : public Vehicle {

public:
	FourWheeler() {
		cout << "Objects with 4 wheels are vehicles" << endl;
	}
};

// Class derived from Derived class 1
class Car : public FourWheeler {

public:
	Car() {
		cout << "Car has 4 wheels" << endl;
	}
};

// main function
int main() {

	Car objCar;
}