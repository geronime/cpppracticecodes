// Program to demonstarte hierarchical inheritance

#include <iostream>
using namespace std;

// Base class
class Parent {

public:
	Parent(char* cpCallingClass) {
		cout << cpCallingClass << endl;
	}
};

// Derived class 1
class Child1 : public Parent {
	
public:
	Child1(char* cpChild1) : Parent(cpChild1) {
		
	}
};

// Derived class 2
class Child2 : public Parent {

public:
	Child2(char* cpChild2) : Parent(cpChild2) {
		
	}
};

// Main function
int main() {
	Child1 objChild1("Parent called from child1");
	Child2 objChild2("Parent called from child2");
}