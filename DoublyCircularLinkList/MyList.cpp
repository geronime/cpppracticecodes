#include "InterFace.h"
#include <stdio.h>
#include <iostream>
#include <string>
using namespace std;

class MyList : List {

private:
	Node *create_Node(Node* nodeToCreate) {
		iCounter++;

		Node *temp = new Node();

		temp->name = nodeToCreate->name;
		temp->city = nodeToCreate->city;
		temp->next = NULL;
		temp->previous = NULL;

		return temp;
	}

	int getPositionOfElement(Node *nodeToInsertAfterOrBefore, bool bIsAfter, bool bIsBefore) {
		int iPosition = 0;
		bool bIsListEmpty = false;

		Node *s;

		if (first == last && first == NULL) {
			return -1;
		}

		s = first;
		if (bIsAfter) {
			for (int i = 0; i < iCounter; i++) {
				iPosition++;
				if (s->name.compare(nodeToInsertAfterOrBefore->name) == 0 && s->city.compare(nodeToInsertAfterOrBefore->city) == 0) {
					bIsListEmpty = true;

					return iPosition;
				}
				s = s->next;
			}
		}

		if (bIsBefore) {
			for (int i = 0; i < iCounter; i++) {
				iPosition++;
				if (s->name.compare(nodeToInsertAfterOrBefore->name) == 0 && s->city.compare(nodeToInsertAfterOrBefore->city) == 0) {
					bIsListEmpty = true;
					return iPosition - 1;
				}
				s = s->next;
			}
		}		

		if (!bIsListEmpty) {
			return -1;
		}

		return 0;
	}

public:
	int iCounter = 0;

	bool add_Node_at_Start(Node *nodeToInsert) {
		cout << endl << "Enter name: " << endl;
		cin >> nodeToInsert->name;

		cout << "Enter City: " << endl;
		cin >> nodeToInsert->city;

		Node *temp;
		temp = create_Node(nodeToInsert);

		// If the list is empty
		if (first == last && first == NULL) {
			first = last = temp;
			first->next = last->next = NULL;
			first->previous = last->previous = NULL;

			cout << endl << "Node inserted in empty list" << endl;
		}
		// If the list is not empty
		else {
			temp->next = first;
			first->previous = temp;
			first = temp;
			first->previous = last;
			last->next = first;
			cout << "Element Inserted at the beginning" << endl;
		}

		return true;
	}

	bool add_Node_After(Node *nodeToInsert, Node *nodeToInsertAfter) {
		int iPosition = 0;

		cout << "Node to add" << endl;
		cout << "Name: " << endl;
		cin >> nodeToInsert->name;

		cout << "City: " << endl;
		cin >> nodeToInsert->city;

		cout << "Node to add after: " << endl;
		cout << "Enter name: " << endl;
		cin >> nodeToInsertAfter->name;

		cout << "Enter city: " << endl;
		cin >> nodeToInsertAfter->city;

		iPosition = getPositionOfElement(nodeToInsertAfter, true, false);

		if (iPosition == -1) {
			cout << "Either the list is empty or the element is not present in the list" << endl;
		}

		Node *temp, *s, *ptr;

		temp = create_Node(nodeToInsert);

		if (first == last && first == NULL) {
			if (iPosition == 1) {
				first = last = temp;
				first->next = last->next = NULL;
				first->previous = last->previous = NULL;
			}
			else {
				cout << "Position entered  is out of range." << endl;
				iCounter--;

				return false;
			}

		}
		else {
			if (iCounter < iPosition) {
				cout << "Position entered  is out of range." << endl;
				iCounter--;

				return false;
			}

			s = first;
			for (int i = 0; i <= iCounter; i++) {
				ptr = s;
				s = s->next;
				if (i == iPosition - 1) {
					ptr->next = temp;
					temp->previous = ptr;
					temp->next = s;
					s->previous = temp;

					cout << "Element inserted successfully at position " << iPosition;

					return true;
				}
			}
		}

		return true;
	}

	bool add_Node_Before(Node *nodeToInsert, Node *nodeToInsertBefore) {
		int iPosition = 0;

		cout << "Node to add" << endl;
		cout << "Name: " << endl;
		cin >> nodeToInsert->name;

		cout << "City: " << endl;
		cin >> nodeToInsert->city;

		cout << "Node to add before: " << endl;
		cout << "Enter name: " << endl;
		cin >> nodeToInsertBefore->name;

		cout << "Enter city: " << endl;
		cin >> nodeToInsertBefore->city;

		iPosition = getPositionOfElement(nodeToInsertBefore, false, true);

		if (iPosition == -1) {
			cout << "Either the list is empty or the element is not present in the list" << endl;
		}

		Node *temp, *s, *ptr;

		temp = create_Node(nodeToInsert);

		if (iPosition == 0) {
			if (first == last && first == NULL) {
				first = last = temp;
				first->next = last->next = NULL;
				first->previous = last->previous = NULL;

				cout << endl << "Element inserted in empty list" << endl;

				return true;
			}
			// If the list is not empty
			else {
				temp->next = first;
				first->previous = temp;
				first = temp;
				first->previous = last;
				last->next = first;
				cout << "Element inserted at position 0" << endl;

				return true;
			}
		}
		
		else {
			if (first == last && first == NULL) {
				if (iPosition == 1) {
					first = last = temp;
					first->next = last->next = NULL;
					first->previous = last->previous = NULL;
				}
				else {
					cout << "Position entered  is out of range." << endl;
					iCounter--;

					return false;
				}

			}
			else {
				if (iCounter < iPosition) {
					cout << "Position entered  is out of range." << endl;
					iCounter--;

					return false;
				}

				s = first;
				for (int i = 0; i <= iCounter; i++) {
					ptr = s;
					s = s->next;
					if (i == iPosition - 1) {
						ptr->next = temp;
						temp->previous = ptr;
						temp->next = s;
						s->previous = temp;

						cout << "Element inserted successfully at position " << iPosition;

						return true;
					}
				}
			}
		}
		
		return true;
	}

	bool delete_Node(Node *nodeToDelete) {
		int iPosition;
		Node *ptr, *s;

		ptr = new Node();

		if (first == last && first == NULL) {
			cout << "List is empty" << endl;

			return 0;
		}

		cout << "Node to delete" << endl;
		cout << "Enter name: " << endl;
		cin >> nodeToDelete->name;

		cout << "Enter city: " << endl;
		cin >> nodeToDelete->city;

		iPosition = getPositionOfElement(nodeToDelete, true, false);

		if (iCounter < iPosition) {
			cout << "Invalid element" << endl;
			return false;
		}

		s = first;
		if (iPosition == 1) {
			iCounter--;
			last->next = s->next;
			s->next->previous = last;
			first = s->next;
			free(s);

			cout << "Node deleted successfully" << endl;
			return true;
		}

		for (int i = 0; i < iPosition - 1; i++) {
			s = s->next;
			ptr = s->previous;
		}
		ptr->next = s->next;
		s->next->previous = ptr;

		if (iPosition == iCounter) {
			last = ptr;
		}

		iCounter--;
		free(s);
		cout << "Node deleted successfully" << endl;

		return true;
	}

	bool display_List() {
		Node *nodeObject;

		if (first == last && first == NULL) {
			cout << "The list is empty" << endl;

			return true;
		}

		nodeObject = first;

		while (nodeObject->next != first) {
			cout << " Name: " << nodeObject->name << " City: " << nodeObject->city << endl;
			nodeObject = nodeObject->next;
		}

		cout << " Name: " << nodeObject->name << " City: " << nodeObject->city << endl;

		return true;
	}
};
