#include "MyList.cpp"
#include <iostream>
using namespace std;

int main() {

	Node *nodeObject = new Node();	
	MyList *myListObject = new MyList();
	Node *nodeToInsert, *nodeToInsertAfter, *nodeToInsertBefore, *nodeToDelete;

	int iChoice = 0;
	int iPosition = 0;

	while (iChoice != 6) {
		cout << endl << "Operations on Doubly Circular linked list" << endl;
		cout << "\n-------------------------------" << endl;
		cout << "1.Insert at Beginning" << endl;
		cout << "2.Insert after" << endl;
		cout << "3.Insert before" << endl;
		cout << "4.Delete Node" << endl;
		cout << "5.Display List" << endl;
		cout << "6.Exit" << endl;

		cout << "Enter your choice : ";
		cin >> iChoice;

		switch (iChoice)
		{
		case 1:
			myListObject->add_Node_at_Start(nodeObject);
			break;

		case 2:
			nodeToInsert = new Node();
			nodeToInsertAfter = new Node();

			myListObject->display_List();

			myListObject->add_Node_After(nodeToInsert, nodeToInsertAfter);
			break;

		case 3:
			nodeToInsert = new Node();
			nodeToInsertBefore = new Node();

			myListObject->display_List();

			myListObject->add_Node_Before(nodeToInsert, nodeToInsertBefore);
			break;

		case 4:
			nodeToDelete = new Node();

			myListObject->delete_Node(nodeToDelete);
			break;

		case 5:
			myListObject->display_List();
			break;

		default:
			break;
		}
	}
	
	return 0;
}