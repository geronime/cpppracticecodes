#include "InterFace.h"
#include <stdio.h>
#include <iostream>
using namespace std;

class MyList : List {

private:
	Node *create_Node(Node* nodeToCreate) {
		Node *temp = new Node();

		temp->name = nodeToCreate->name;
		temp->city = nodeToCreate->city;
		temp->next = NULL;
		temp->previous = NULL;

		return temp;
	}

public:
	bool add_Node_at_Start(Node *nodeToInsert) {
		cout << endl << "Enter name: " << endl;
		cin >> nodeToInsert->name;

		cout << "Enter City: " << endl;
		cin >> nodeToInsert->city;

		Node *temp;
		temp = create_Node(nodeToInsert);

		// If the list is empty
		if (first == last && first == NULL) {
			first = last = temp;
			first->next = last->next = NULL;
			first->previous = last->previous = NULL;

			cout << endl << "Node inserted in empty list" << endl;
		}
		// If the list is not empty
		else {
			temp->next = first;
			first->previous = temp;
			first = temp;
			first->previous = last;
			last->next = first;
			cout << "Element Inserted at the beginning" << endl;
		}

		return true;
	}

	bool add_Node_After(Node, Node) {
		return true;
	}

	bool add_Node_Before(Node, Node) {
		return true;
	}

	bool delete_Node(Node) {
		return true;
	}

	bool display_List() {
		return true;
	}
};
