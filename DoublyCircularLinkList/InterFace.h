#include "Node.h"
#include <stdio.h>

class List :public Node
{
protected:
	//required Variables
	Node *first = NULL;
	Node *last = NULL;
	Node *current = NULL;

public:
	virtual Node *create_Node(Node*) = 0; //int provides number of starting nodes
	virtual bool add_Node_at_Start(Node*) = 0; //node is the value to be inserted
	virtual bool add_Node_After(Node *, Node *) = 0;//First node is the value while Second node is the node after which node is to be inserted
	virtual bool add_Node_Before(Node *, Node *) = 0;//Similar as above
	virtual bool delete_Node(Node *) = 0;//Should Consist of value of the node
	virtual bool display_List() = 0;	
};
