// Program to demonstrate implementation of Linked List

#include <stdio.h>
#include <stdlib.h>

// Declare a linked list
struct LinkedList{
	int data;
	struct LinkedList *next;
};

// Defining node as pointer of data type struct Linkedlist
typedef struct LinkedList *node;

// Method to create node in the linked list
node createNode() {
	node temp;
	temp = (node)malloc(sizeof(struct LinkedList)); // memory allocation using malloc
	temp->next = NULL; // make next point to NULL

	return temp;
}

// Method to add a node to the linked list
node addNode(node head, int value) {
	node temp, p; // Declare two nodes
	temp = createNode(); // createNode() will return a node with data=value and next pointing to NULL
	temp->data = value;
	
	if (head == NULL) {
		head = temp; // when the linked list is empty
	}
	else {
		p = head;
		while (p->next != NULL) {
			p = p->next; // traverse the list till p is the last node
		}
		p->next = temp;
	}

	printf("Inserted %d\n", value);
	return head;
}

int main() {
	node head = NULL;
	head = addNode(head, 15);
	head = addNode(head, 16);

	while (head != NULL) {
		printf("%d\n", head->data);
		head = head->next;
	}
}