// Program to demonstrate Runtime Polymorphism

#include <iostream>
using namespace std;

// Base class
class Parent {

public:
	void print() {
		cout << "This function is of Parent Class" << endl;
	}
};

// Derived class
class Child : public Parent {

public:
	void print() {
		cout << "This function is of Child Class" << endl;
	}
};

// Main function
int main() {
	// Object of Parent class
	Parent objParent;

	// Object of Child class
	Child objChild = Child();

	// Print function of parent class
	objParent.print();

	// Print function of child class
	objChild.print();

	return 0;
}