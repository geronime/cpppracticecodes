// Program to demonstrate Complie Time Polymorphism
// Function Overloading

#include <iostream>
using namespace std;

class MyClass {

public:
	// Function with one int as a parameter
	void someFunction(int iAge) {
		cout << "Age = " << iAge << endl;
	}

	// Function with one double as a parameter
	void someFunction(double dHeight) {
		cout << "Height = " << dHeight << endl;
	}

	// Function with a pointer to char as a paramater
	void someFunction(char* cpPersonName) {
		cout << "Name = " << cpPersonName << endl;
	}
};

// Main function
int main() {

	MyClass objMyClass;
	
	objMyClass.someFunction("Prasanna");
	objMyClass.someFunction(23);
	objMyClass.someFunction(5.11);
}